# Indigo Spring Boot WS

Indigo exercise, CRUD of books in spring boot.
<br/>

## Prerequisites
- JDK 8
- Maven 3.3.9 or higher.

## Build
```bash
mvn clean install -U 
```

## Run with java -jar command
```bash
java -jar target/bookstore-ws-1.0.0-SNAPSHOT.jar
```

## Run with mvn
```bash
mvn spring-boot:run
```

## Modules
- **bookstore-api:** Contains REST implementation for bookstore.
- **bookstore-dao:** Contains the data access object.
- **bookstore-dto:** Contains all the model classes.
- **bookstore-service:** Contains the logic class.
- **bookstore-util:** Contains the util class.

## Deployment

This service can be deployed in apache tomcat server.

The endpoint that this service create is: 
```bash
http://localhost:8080/
```

## Methods of the api

- **list:** Get All the books.
- **create:** Create a new book.(A validation that not accept the same name.)
- **edit:** Edit a register.
- **getById:** Get a book by its identifier.
- **remove:** Delete a register.

## Model of the book
```bash
{
    "id": 1,
    "name": "El arte del amor",
    "author": "Erich From",
    "pages": 356,
    "editorial": "Presti"
}
```
## Cross Origin acces
```bash
http://localhost:4200
```