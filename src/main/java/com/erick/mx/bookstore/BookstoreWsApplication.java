/*
 *      File: BookstoreWsConfiguration.java
 *    Author: Erick Garcia <erick.isc.garcia@gmail.com>
 *      Date: May 22, 2019
 * Copyright: Garbed Technologies. 2019
 */
package com.erick.mx.bookstore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

/**
 * Class that represents the bookstore application.
 *
 * @author Erick Garcia &lt;erick.garcia@dsindigo.mx&gt;
 * @version 1.0.0
 * @since 1.0.0
 */
@SpringBootApplication
@Import(BookstoreWsConfiguration.class)
public class BookstoreWsApplication {

    public static void main(final String[] args) {
        SpringApplication.run(BookstoreWsApplication.class, args);
    }
}
