/*
 *      File: BookstoreWsConfiguration.java
 *    Author: Erick Garcia <erick.isc.garcia@gmail.com>
 *      Date: May 22, 2019
 * Copyright: Garbed Technologies. 2019
 */
package com.erick.mx.bookstore;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.async.DeferredResult;

import static springfox.documentation.builders.PathSelectors.regex;
import static springfox.documentation.schema.AlternateTypeRules.newRule;

import com.erick.mx.bookstore.properties.SwaggerProperty;
import com.fasterxml.classmate.TypeResolver;
import com.google.common.base.Predicate;
import java.time.LocalDate;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.WildcardType;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.OperationsSorter;
import springfox.documentation.swagger.web.UiConfiguration;
import springfox.documentation.swagger.web.UiConfigurationBuilder;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * This is the class for .
 *
 * @author Erick Garcia &lt;erick.isc.dante@gmail.com&gt;
 * @version 1.0.0
 * @since 1.0.0
 */
@Configuration
@ComponentScan
@EnableSwagger2
public class BookstoreWsConfiguration {

    /**
     * Type resolver.
     */
    @Autowired
    private TypeResolver typeResolver;


    /**
     * Swagger properties configuration.
     */
    @Autowired
    private SwaggerProperty swaggerProperty;

    /**
     * Method to build the swagger information.
     *
     * @return The Docket object.
     */
    @Bean
    public Docket swaggerApi() {
        return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo()).select().apis(RequestHandlerSelectors.any())
                .paths(paths()).build()
                .pathMapping(swaggerProperty.getPathMapping())
                .directModelSubstitute(LocalDate.class, String.class)
                .genericModelSubstitutes(ResponseEntity.class)
                .alternateTypeRules(newRule(
                        typeResolver.resolve(DeferredResult.class,
                                typeResolver.resolve(ResponseEntity.class, WildcardType.class)),
                        typeResolver.resolve(WildcardType.class)))
                .useDefaultResponseMessages(false)
                .tags(new Tag(swaggerProperty.getTag().getName(), swaggerProperty.getTag().getDescription()));
    }

    /**
     * Method to build the swagger configuration.
     *
     * @return The UiConfiguration.
     */
    @Bean
    @ConditionalOnProperty("swagger.ui.enabled")
    public UiConfiguration uiConfig() {
        return UiConfigurationBuilder.builder()
                .displayOperationId(true)
                .displayRequestDuration(true)
                .validatorUrl(swaggerProperty.getConfig().getValidator())
                .operationsSorter(OperationsSorter.ALPHA)
                .showExtensions(true)
                .build();
    }

    /**
     * Method to generate the API information.
     *
     * @return The Api information.
     */
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder().title(swaggerProperty.getTitle())
                .description(swaggerProperty.getDescription()).version(swaggerProperty.getVersion())
                .contact(new Contact(swaggerProperty.getContact().getName(), swaggerProperty.getContact().getUrl(), swaggerProperty.getContact().getEmail()))
                .build();
    }

    /**
     * Regex Path.
     *
     * @return The predicate regex path.
     */
    private Predicate<String> paths() {
        return regex(swaggerProperty.getRegexPath());
    }
}
