/*
 *      File: package-info.java
 *    Author: Erick Garcia <erick.isc.garcia@gmail.com>
 *      Date: May 22, 2019
 * Copyright: Garbed Technologies. 2019
 */
/**
 * This package contains all the configuration for the bookstore application.
 *
 * @author Erick Garcia &lt;erick.isc.dante@gmail.com&gt;
 * @version 1.0.0
 * @since 1.0.0
 */
package com.erick.mx.bookstore;