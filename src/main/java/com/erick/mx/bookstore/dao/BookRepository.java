/*
 *      File: BookRepository.java
 *    Author: Erick Garcia <erick.isc.dante@gmail.com>
 *      Date: Oct 11, 2018
 */
package com.erick.mx.bookstore.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.erick.mx.bookstore.dto.Book;

/**
 * The interface for the book repository methods.
 */
public interface BookRepository extends JpaRepository<Book, Long> {

    /**
     * Method to get a book by it's name.
     *
     * @param name The name book.
     * @return A book associated to the name provided.
     */
    public Book getByName(String name);

}
