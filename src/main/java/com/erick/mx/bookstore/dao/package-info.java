/*
 *      File: package-info.java
 *    Author: Erick Garcia <erick.isc.dante@gmail.com>
 *      Date: oct 11, 2018
 */
/**
 * This package contains all de data access objects.
 */
package com.erick.mx.bookstore.dao;