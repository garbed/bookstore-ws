package com.erick.mx.bookstore.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.erick.mx.bookstore.dao.BookRepository;
import com.erick.mx.bookstore.dto.Book;
import com.erick.mx.bookstore.service.BookService;

@Service
public class BookServiceImpl implements BookService {

	@Autowired
	BookRepository dao;

	@Override
	public List<Book> getAll() {
		return dao.findAll();
	}

	@Override
	public Book save(Book book) {
		return dao.save(book);
	}

	@Override
	public Book update(Book book) {
		return dao.saveAndFlush(book);
	}

	@Override
	public Optional<Book> getById(Long id) {
		return dao.findById(id);
	}

	@Override
	public void delete(Long id) {
		dao.deleteById(id);
	}

	@Override
	public Book getByName(String name) {
		return dao.getByName(name);
	}
	
	@Override
	public boolean bookExist(String name) {
		return getByName(name)!=null;
	}

}
