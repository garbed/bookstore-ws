/*
 *      File: package-info.java
 *    Author: Erick Garcia <erick.isc.dante@gmail.com>
 *      Date: Oct 11, 2018
 */
/**
 * This package contains all the interfaces for the business logic.
 */
package com.erick.mx.bookstore.service;