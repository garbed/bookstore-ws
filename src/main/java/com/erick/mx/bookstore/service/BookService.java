/*
 *      File: BookService.java
 *    Author: Erick Garcia <erick.isc.dante@gmail.com>
 *      Date: Oct 11, 2018
 */
package com.erick.mx.bookstore.service;

import java.util.List;
import java.util.Optional;

import com.erick.mx.bookstore.dto.Book;

/**
 * Interface for business logic.
 */
public interface BookService {

    /**
     * Get all the books.
     *
     * @return The list of books.
     */
    public List<Book> getAll();

    /**
     * Create a new book.
     *
     * @param book Book information.
     * @return The data of book created.
     */
    public Book save(Book book);

    /**
     * Update the book information.
     *
     * @param book The new information.
     * @return The new information.
     */
    public Book update(Book book);

    /**
     * Get a book by it's identifier.
     *
     * @param id The identifier.
     * @return The book associated to the identifier provided.
     */
    Optional<Book> getById(Long id);

    /**
     * Delete a book.
     *
     * @param id The identifier.
     */
    public void delete(final Long id);

    /**
     * Get a book by it's name.
     *
     * @param name The name of the book.
     * @return A book associated to the name provided.
     */
    public Book getByName(final String name);

    /**
     * Validate if a book whit the name provided already exist.
     *
     * @param name The name to validate.
     * @return True if already exist or false otherwise.
     */
    public boolean bookExist(String name);
}