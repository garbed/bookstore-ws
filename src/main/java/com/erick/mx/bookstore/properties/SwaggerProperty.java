/*
 *      File: SwaggerProperty.java
 *    Author: Erick Garcia <erick.isc.garcia@gmail.com>
 *      Date: May 22, 2019
 * Copyright: Garbed Technologies. 2019
 */
package com.erick.mx.bookstore.properties;

import javax.validation.constraints.NotNull;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

import lombok.Data;

/**
 * Class for the swagger configuration.
 *
 * @author Erick Garcia &lt;erick.garcia@dsindigo.mx&gt;
 * @version 1.0.0
 * @since 1.0.0
 */
@Configuration
@ConfigurationProperties("swagger.metadata")
@Data
@Validated
public class SwaggerProperty {

    /**
     * Tag block.
     */
    @Data
    @Validated
    public static class Tag {
        /**
         * Name.
         */
        @NotNull
        private String name;
        /**
         * Description.
         */
        @NotNull
        private String description;
    }

    /**
     * Contact block.
     */
    @Data
    @Validated
    public static class Contact {
        /**
         * Contact name.
         */
        @NotNull
        private String name;
        /**
         * Contact url.
         */
        @NotNull
        private String url;
        /**
         * Contact email.
         */
        @NotNull
        private String email;
    }

    /**
     * Config block.
     */
    @Data
    @Validated
    public static class Config {
        /**
         * Validator value.
         */
        @NotNull
        private String validator;
    }

    /**
     * Regex path.
     */
    @NotNull
    private String regexPath;

    /**
     * Path mapping.
     */
    @NotNull
    private String pathMapping;

    /**
     * Tag.
     */
    @NotNull
    private Tag tag;

    /**
     * Config.
     */
    @NotNull
    private Config config;

    /**
     * Title.
     */
    @NotNull
    private String title;

    /**
     * Description.
     */
    @NotNull
    private String description;

    /**
     * Version.
     */
    @NotNull
    private String version;

    /**
     * Contact object.
     */
    @NotNull
    private Contact contact;
}
