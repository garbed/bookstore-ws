/*
 *      File: Book.java
 *    Author: Erick Garcia <erick.isc.dante@gmail.com>
 *      Date: Oct 11, 2018
 */
package com.erick.mx.bookstore.dto;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Entity class of book.
 */
@Entity
@NamedQuery(name = "Book.getByName", query = "SELECT b FROM Book b WHERE LOWER(b.name) = LOWER(?1)")
public class Book implements Serializable {


    /**
     * Serial version UID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Book identifier.
     */
    @Id
    @GeneratedValue
    private long id;

    /**
     * Book name.
     */
    @NotNull(message = "El nombre es requerido")
    @Size(min = 2, max = 30, message = "El nombre debe tener entre {min} y {max} caracteres")
    private String name;

    /**
     * Book author.
     */
    @NotNull(message = "El autor es requerido")
    @Size(min = 2, max = 30, message = "El nombre del autor debe tener entre {min} y {max} caracteres")
    private String author;

    /**
     * Pages.
     */
    @Min(1)
    private int pages;

    /**
     * Editorial.
     */
    @NotNull(message = "El editorial es requerido")
    @Size(min = 2, max = 50, message = "El nombre del editorial debe tener entre {min} y {max} caracteres")
    private String editorial;

    /**
     * Create a new instance of {@code Book}.
     */
    public Book() {

    }

    /**
     * Create a new instance of {@code Book}.
     *
     * @param id        Identifier.
     * @param name      Name.
     * @param author    Author.
     * @param pages     Pages.
     * @param editorial Editorial.
     */
    public Book(final long id, final String name, final String author, final int pages, final String editorial) {
        super();
        this.id = id;
        this.name = name;
        this.author = author;
        this.pages = pages;
        this.editorial = editorial;
    }

    public long getId() {
        return id;
    }

    public void setId(final long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(final String author) {
        this.author = author;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(final int pages) {
        this.pages = pages;
    }

    public String getEditorial() {
        return editorial;
    }

    public void setEditorial(final String editorial) {
        this.editorial = editorial;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((author == null) ? 0 : author.hashCode());
        result = prime * result + ((editorial == null) ? 0 : editorial.hashCode());
        result = prime * result + (int) (id ^ (id >>> 32));
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + pages;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Book other = (Book) obj;
        if (author == null) {
            if (other.author != null) {
                return false;
            }
        } else if (!author.equals(other.author)) {
            return false;
        }
        if (editorial == null) {
            if (other.editorial != null) {
                return false;
            }
        } else if (!editorial.equals(other.editorial)) {
            return false;
        }
        if (id != other.id) {
            return false;
        }
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        if (pages != other.pages) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Book [id=" + id + ", name=" + name + ", author=" + author + ", pages=" + pages + ", editorial="
                + editorial + "]";
    }


}
