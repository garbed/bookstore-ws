/*
 *      File: CustomErrorjava
 *    Author: Erick Garcia <erick.isc.dante@gmail.com>
 *      Date: Oct 11, 2018
 */
package com.erick.mx.bookstore.util;

/**
 * Custom error class for set message in response entity.
 */
public class CustomError {

    private String errorMessage;

    public CustomError(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

}