/*
 *      File: BooksApi.java
 *    Author: Erick Garcia <erick.isc.dante@gmail.com>
 *      Date: Oct 11, 2018
 */
package com.erick.mx.bookstore.api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.erick.mx.bookstore.dto.Book;
import com.erick.mx.bookstore.service.BookService;
import com.erick.mx.bookstore.util.CustomError;

/**
 * The Api class.
 *
 * @author Erick Garcia &lt;erick.isc.dante@gmail.com&gt;
 * @version 1.0.0
 * @since 1.0.0
 */
@CrossOrigin(origins = "http://localhost:4200")
@Api(tags = "Bookstore API:")
@Validated
@RestController
@RequestMapping(value = "/bookstore")
public class BooksApi {

    /**
     * Class logger.
     */
    public static final Logger LOGGER = LoggerFactory.getLogger(BooksApi.class);

    /**
     * Book Service.
     */
    @Autowired
    BookService bookService;

    /**
     * Method to get all the books.
     *
     * @return The list of books or an error message if there are not elements.
     */
    @ApiOperation(value = "Obtains all the books.")
    @ApiResponses( {
            @ApiResponse(code = 200, message = "Operation completed successfully."),
            @ApiResponse(code = 400, message = "Invalid parameters"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @GetMapping(value = "/list", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<List<Book>> getall() {
        LOGGER.info("Get all books");
        final List<Book> books;
        try {
            books = bookService.getAll();
            if (books.isEmpty()) {
                return new ResponseEntity(new CustomError("Resources not found"), HttpStatus.NOT_FOUND);

            }
        } catch (Exception e) {
            LOGGER.error("Error searching books.", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(books, HttpStatus.OK);
    }

    /**
     * Method to create a new book.
     *
     * @param book The book object to be created.
     * @return The information of the book created.
     */
    @ApiOperation(value = "Create a new book.")
    @ApiResponses( {
            @ApiResponse(code = 200, message = "Operation completed successfully."),
            @ApiResponse(code = 400, message = "Invalid parameters"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @PostMapping(value = "/create", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Book> save(@ApiParam(value = "The book entity.", required = true) @Valid @RequestBody final Book book) {
        LOGGER.info("Creating book : {}", book);
        try {
            if (bookService.bookExist(book.getName())) {
                LOGGER.error("Unable to create. A book with name {}, already exist", book.getName());
                return new ResponseEntity(
                        new CustomError("Unable to create. User with name: " + book.getName() + " book already exist."),
                        HttpStatus.CONFLICT);
            }
            bookService.save(book);
        } catch (Exception e) {
            LOGGER.error("Error create a new book.", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(book, HttpStatus.CREATED);
    }

    /**
     * Method to edit the book
     *
     * @param id   The book identifier.
     * @param book The book information.
     * @return The information of the book edited.
     */
    @ApiOperation(value = "Edit a book by identifier.")
    @ApiResponses( {
            @ApiResponse(code = 200, message = "Operation completed successfully."),
            @ApiResponse(code = 400, message = "Invalid parameters"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @PutMapping(value = "/edit/{id}")
    public ResponseEntity<Book> update(@ApiParam(value = "The book identifier.",
            required = true, example = "1") @PathVariable final Long id, @ApiParam(value = "The data for the edition..",
            required = true) @Valid @RequestBody final Book book) {

        LOGGER.info("Updating book with id {}", id);
        try {

            final Optional<Book> currentBook = bookService.getById(id);

            if (!currentBook.isPresent()) {
                LOGGER.error("Unable to update. Book with id {} not found.", id);
                return new ResponseEntity(new CustomError("Unable to update. Book with id " + id + " not found."),
                        HttpStatus.NOT_FOUND);
            }

            book.setId(currentBook.get().getId());

            bookService.update(book);
        } catch (final Exception e) {
            LOGGER.error("Error edit a book.", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(book, HttpStatus.OK);

    }

    /**
     * Method to get a book by it's identifier.
     *
     * @param id The book identifier.
     * @return The book associated to the identifier provided.
     */
    @ApiOperation(value = "Obtains a book by identifier.")
    @ApiResponses( {
            @ApiResponse(code = 200, message = "Operation completed successfully."),
            @ApiResponse(code = 400, message = "Invalid parameters"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @GetMapping(value = "/getById/{id}")
    public ResponseEntity<Book> getById(@ApiParam(value = "The book identifier.",
            required = true, example = "1") @PathVariable final Long id) {
        LOGGER.info("Get a book by its identifier {}", id);
        final Book book;
        try {
            final Optional<Book> optionalBook = bookService.getById(id);

            if (!optionalBook.isPresent()) {
                LOGGER.error("Book with id {} not found.", id);
                return new ResponseEntity(new CustomError("Unable to find a book with id " + id), HttpStatus.NOT_FOUND);
            }

            book = optionalBook.get();
        } catch (final Exception e) {
            LOGGER.error("Error searching a book by it's identifier.", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(book, HttpStatus.OK);
    }

    /**
     * Method to delete a book.
     *
     * @param id The book identifier.
     * @return A Http response code.
     */
    @ApiOperation(value = "Delete a book by its identifier.")
    @ApiResponses( {
            @ApiResponse(code = 200, message = "Operation completed successfully."),
            @ApiResponse(code = 400, message = "Invalid parameters"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @DeleteMapping(value = "/remove/{id}")
    public ResponseEntity<Book> delete(@ApiParam(value = "The book identifier.",
            required = true, example = "1") @PathVariable Long id) {
        LOGGER.info("Deleting book by id {}", id);
        try {
            final Optional<Book> bookOptional = bookService.getById(id);
            if (!bookOptional.isPresent()) {
                LOGGER.error("Unable to delete. Book with id {} not found.", id);
                return new ResponseEntity(new CustomError("Unable to delete. Book with id " + id + " not found."),
                        HttpStatus.NOT_FOUND);
            }
            bookService.delete(bookOptional.get().getId());
        } catch (final Exception e) {
            LOGGER.error("Error deleting a book.", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
