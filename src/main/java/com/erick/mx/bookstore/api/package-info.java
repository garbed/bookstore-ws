/*
 *      File: package-info.java
 *    Author: Erick Garcia <erick.isc.dante@gmail.com>
 *      Date: Oct 11, 2018
 */
/**
 * This package contains the Api resources.
 */
package com.erick.mx.bookstore.api;